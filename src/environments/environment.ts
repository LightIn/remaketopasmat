// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyCLDBJfKz5CAaqBbw1yLybiNFlrsILz4MU",
    authDomain: "angularblog-b3785.firebaseapp.com",
    databaseURL: "https://angularblog-b3785.firebaseio.com",
    projectId: "angularblog-b3785",
    storageBucket: "angularblog-b3785.appspot.com",
    messagingSenderId: "419560678124",
    appId: "1:419560678124:web:0ab3e94c8c685537a43bc9"
  }

};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
