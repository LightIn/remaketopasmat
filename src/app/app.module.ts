// CORE IMPORT
import {AppComponent} from './app.component';
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';


// EXTERNAL IMPORT
import {ChartsModule} from 'ng2-charts';
import {MatCardModule} from '@angular/material/card';
import {MatButtonModule} from '@angular/material/button';
import {MatDividerModule} from '@angular/material/divider';
import {MatIconModule} from '@angular/material/icon';
import {MatChipsModule} from '@angular/material/chips';
import {MatRadioModule} from '@angular/material/radio';
import {AgGridModule} from 'ag-grid-angular';
import { MatListModule } from '@angular/material/list';


// DATEPICKER IMPORT
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatInputModule} from '@angular/material/input';
import {MatNativeDateModule} from '@angular/material/core';

// FIREBASE IMPORT
import {environment} from '../environments/environment';
import {AngularFireModule} from '@angular/fire';
import {AngularFireDatabaseModule} from '@angular/fire/database';
import {AngularFirestoreModule} from '@angular/fire/firestore';
import {AngularFireStorageModule} from '@angular/fire/storage';
import {AngularFireMessagingModule} from '@angular/fire/messaging';


// COMPONENT IMPORT
import {CardComponent} from './components/card/card.component';
import {BoutonComponent} from './components/bouton/bouton.component';
import {ProfilComponent} from './components/profil/profil.component';
import {BarreMenuComponent} from './components/barre-menu/barre-menu.component';
import {BarreRechercheComponent} from './components/barre-recherche/barre-recherche.component';
import {GraphiqueComponent} from './components/graphique/graphique.component';
import {DemandeComponent} from './components/demande/demande.component';
import {CalendrierComponent} from './components/calendrier/calendrier.component';
import {MessageComponent} from './components/message/message.component';
import {MapComponent} from './components/map/map.component';
import {DialogBoxComponent} from './components/dialog-box/dialog-box.component';
import {FormInputComponent} from './components/form-input/form-input.component';
import {SideMenuComponent} from './components/side-menu/side-menu.component';
import {MessagerieComponent} from './views/messagerie/messagerie.component';
import {ConversationComponent} from './views/conversation/conversation.component';
import {NotificationComponent} from './components/notification/notification.component';
import {GridComponent} from './components/grid/grid.component';
import {MessageBulleComponent} from './components/message-bulle/message-bulle.component';
import {MessageBottomBarComponent} from './components/message-bottom-bar/message-bottom-bar.component';
import {EnfantComponent} from './components/enfant/enfant.component';
import {CompProfileComponent} from './components/comp-profile/comp-profile.component';
import {EventComponent} from './components/event/event.component';
import {FormEventComponent} from './components/form-event/form-event.component';
import {DisplayDialogboxComponent} from './views/display-dialogbox/display-dialogbox.component';
import {ConvEntryComponent} from './components/conv-entry/conv-entry.component';


// VIEW IMPORT
import {DashboardComponent} from './views/dashboard/dashboard.component';
import {ListeEnfantsComponent} from './views/liste-enfants/liste-enfants.component';
import {JournalActiviteComponent} from './views/journal-activite/journal-activite.component';
import {PageNotifComponent} from './views/page-notif/page-notif.component';
import {StatistiqueComponent} from './views/statistique/statistique.component';
import {OffreComponent} from './views/offre/offre.component';
import {LoginComponent} from './views/login/login.component';
import {RegisterComponent} from './views/register/register.component';
import {PlanningComponent} from './views/planning/planning.component';
import {CalendrierComponent1} from './views/calendrier/calendrier.component';
import {SettingsComponent} from './views/settings/settings.component';
import {ProfileViewComponent} from './views/profil/profil.component';
import {MailConfirmComponent} from './views/mail-confirm/mail-confirm.component';
import {MatchingComponent} from './views/matching/matching.component';
import {MatchProfileComponent} from './views/match-profile/match-profile.component';
import {Error404Component} from './views/error404/error404.component';
import {MatchAcceptComponent} from './views/match-accept/match-accept.component';
import { OtherProfileComponent } from './views/other-profile/other-profile.component';
import { AjouterEnfantComponent } from './views/ajouter-enfant/ajouter-enfant.component';


// SERVICE IMPORT
import {AuthentificationService} from './services/authentification.service';

import {OffreService} from './services/offre.service';
import {MatchingService} from './services/matching.service';
import {EnfantsService} from './services/enfants.service';
import {MessagerieService} from './services/messagerie.service';




// GUARD IMPORT
import {AuthGuardGuard} from './guard/auth-guard.guard';
import {CalendarModule} from 'angular-calendar';
import {SchedulerModule} from 'angular-calendar-scheduler';
import {jqxSchedulerModule} from 'jqwidgets-ng/jqxscheduler';




const routes: Routes = [
  {path: '', component: LoginComponent},
  {path: 'dashboard', component: DashboardComponent, canActivate: [AuthGuardGuard]},
  {path: 'calendrier', component: CalendrierComponent, canActivate: [AuthGuardGuard]},
  {path: 'journal', component: JournalActiviteComponent, canActivate: [AuthGuardGuard]},
  {path: 'listeEnfants', component: ListeEnfantsComponent},
  {path: 'map', component: MapComponent, canActivate: [AuthGuardGuard]},
  {path: 'offre', component: OffreComponent, canActivate: [AuthGuardGuard]},
  {path: 'notifications', component: PageNotifComponent, canActivate: [AuthGuardGuard]},
  {path: 'profile', component: ProfileViewComponent, canActivate: [AuthGuardGuard]},
  {path: 'profile/:id', component: OtherProfileComponent, canActivate: [AuthGuardGuard]},
  {path: 'register', component: RegisterComponent},
  {path: 'listeEnfants/ajouterEnfant', component: AjouterEnfantComponent, canActivate: [AuthGuardGuard] },
  {path: 'stats', component: StatistiqueComponent, canActivate: [AuthGuardGuard]},
  {path: 'button', component: BoutonComponent, canActivate: [AuthGuardGuard]},
  {path: 'settings', component: SettingsComponent, canActivate: [AuthGuardGuard]},
  {path: 'side', component: SideMenuComponent, canActivate: [AuthGuardGuard]},
  {path: 'demande', component: DemandeComponent, canActivate: [AuthGuardGuard]},
  {path: 'messages', component: MessagerieComponent, canActivate: [AuthGuardGuard]},
  {path: 'messages/:id', component: ConversationComponent, canActivate: [AuthGuardGuard]},
  {path: 'confirm', component: MailConfirmComponent, canActivate: [AuthGuardGuard]},
  {path: 'matching', component: MatchingComponent, canActivate: [AuthGuardGuard]},
  {path: 'matching/accept', component: MatchAcceptComponent, canActivate: [AuthGuardGuard]},
  {path: 'matching/:id', component: MatchProfileComponent, canActivate: [AuthGuardGuard]},
  {path: '**', component: Error404Component},
  {path: 'planning', component: PlanningComponent},
  {path: 'ajout-event', component: FormEventComponent},
  {path: 'dialog', component: DialogBoxComponent},

];  // sets up routes constant where you define your routes


@NgModule({
  declarations: [
    AppComponent,
    BoutonComponent,
    ProfilComponent,
    BarreMenuComponent,
    BarreRechercheComponent,
    GraphiqueComponent,
    DemandeComponent,
    CalendrierComponent,
    MessageComponent,
    MapComponent,
    DashboardComponent,
    ListeEnfantsComponent,
    JournalActiviteComponent,
    PageNotifComponent,
    StatistiqueComponent,
    OffreComponent,
    LoginComponent,
    RegisterComponent,
    CardComponent,
    DialogBoxComponent,
    FormInputComponent,
    SideMenuComponent,
    SettingsComponent,
    MessagerieComponent,
    ConversationComponent,
    ProfileViewComponent,
    NotificationComponent,
    GridComponent,
    MailConfirmComponent,
    MessageBulleComponent,
    MessageBottomBarComponent,
    PlanningComponent,
    CalendrierComponent1,
    MatchingComponent,
    MatchProfileComponent,
    EnfantComponent,
    Error404Component,
    MatchAcceptComponent,
    OtherProfileComponent,
    CompProfileComponent,
    EventComponent,
    FormEventComponent,
    DisplayDialogboxComponent,
    AjouterEnfantComponent,
    ConvEntryComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    RouterModule.forRoot(routes),
    ChartsModule,
    MatCardModule,
    MatButtonModule,
    MatDividerModule,
    BrowserAnimationsModule,
    AgGridModule.withComponents([]),
    MatDatepickerModule,
    MatInputModule,
    MatNativeDateModule,
    MatIconModule,
    ReactiveFormsModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule,
    AngularFireDatabaseModule,
    AngularFireStorageModule,
    ReactiveFormsModule,
    MatChipsModule,
    MatRadioModule,
    AngularFireMessagingModule,
    MatListModule,
    FormsModule

  ],


  entryComponents: [DialogBoxComponent],

  providers: [AuthentificationService, AuthGuardGuard, OffreService, MatchingService, EnfantsService, MessagerieService],


  bootstrap: [AppComponent]
})
export class AppModule {

}
