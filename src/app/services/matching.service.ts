import {Injectable} from '@angular/core';
import {AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument} from '@angular/fire/firestore';

import {Conversation, Match, User} from '../models/models';

import {Router} from '@angular/router';
import {Observable} from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class MatchingService {


  constructor(
    public firestore: AngularFirestore,
    private router: Router,
  ) {


  }


  private itemsCollection: AngularFirestoreCollection<User>;
  items: Observable<User[]>;

  GetMatchingUser(AssValue: boolean) {


    this.itemsCollection = this.firestore.collection<User>('users', ref => ref.where('AssMat', '==', !AssValue));
    this.items = this.itemsCollection.valueChanges();
    return this.items;

  }


  getProfile(id: string) {

    let itemDoc: AngularFirestoreDocument<User>;
    let item: Observable<User>;


    itemDoc = this.firestore.doc<User>('users/' + id);
    item = itemDoc.valueChanges();
    item.subscribe(next => console.log('signle : ' + next.displayName));
    return item;
  }


  askFriend(TargetId: string, SenderId: string) {

    console.log('Target : ' + TargetId + ' et Sender : ' + SenderId);
    let itemDoc: AngularFirestoreDocument<Match>;
    let newMatch: Match;
    itemDoc = this.firestore.doc<Match>('matchs/' + TargetId + SenderId);
    newMatch = {
      owner: TargetId,
      requests: SenderId,
    };
    itemDoc.set(newMatch);
  }


  checkIfMachAsk(id: string): Observable<Match[]> {
    const myid: string = JSON.parse(localStorage.getItem('User')).uid;
    console.log('check for : ' + id + myid);
    return this.firestore.collection<Match>('matchs', ref => ref
      .where('owner', '==', id).where('requests', '==', myid)).valueChanges();

  }


  getFriendRequests(id: string): Observable<Match[]> {
    let itemsCollection: AngularFirestoreCollection<Match>;
    let items: Observable<Match[]>;
    itemsCollection = this.firestore.collection<Match>('matchs', ref => ref.where('owner', '==', id));
    items = itemsCollection.valueChanges();
    return items;

  }


  async addFriend(AcceptId: string) {

    const myid = JSON.parse(localStorage.getItem('User')).uid;
    let Ffriends: string[];


    const userRef1: AngularFirestoreDocument<User> = await this.firestore.doc(`users/${AcceptId}`);
    userRef1.ref.get().then(snap => {
      Ffriends = snap.get('friends');
      Ffriends.push(myid);
      userRef1.update({
        friends: Ffriends
      });
    });


    let userRef2: AngularFirestoreDocument<User> = await this.firestore.doc(`users/${myid}`);
    userRef2.ref.get().then(snap => {
      Ffriends = snap.get('friends');
      Ffriends.push(AcceptId);
      userRef2.update({
        friends: Ffriends
      });
    });

    await this.firestore.doc(`matchs/${myid + AcceptId}`).delete();


    this.createConversation(AcceptId, myid);

  }


  createConversation(pers1: string, pers2: string) {
    const uid = this.firestore.createId();

    const conv: Conversation = {
      participant: [],
      lastMessage: null,
      lastMessageDate: new Date(),
      id: uid,
    };
    conv.participant.push(pers1);
    conv.participant.push(pers2);

    this.firestore.doc('convs/' + uid).set(conv);
    console.log('Conv');

  }


  cancelFriend(TargetId: string, SenderId: string) {
    let itemDoc: AngularFirestoreDocument<Match>;
    itemDoc = this.firestore.doc<Match>('matchs/' + TargetId + SenderId);
    itemDoc.delete();
  }


}


