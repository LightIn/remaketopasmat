import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from '@angular/fire/firestore';
import { Enfant } from '../models/models';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EnfantsService {

  constructor(public firestore: AngularFirestore) { }

  private itemsCollection: AngularFirestoreCollection<Enfant>;
  items: Observable<Enfant[]>;

  GetEnfants() {
    this.itemsCollection = this.firestore.collection<Enfant>('enfants');
    this.items = this.itemsCollection.valueChanges();
    return this.items;
  }

  AddEnfant(newEnfant) {
    const id = this.firestore.createId();
    const enfantRef: AngularFirestoreDocument<any> = this.firestore.doc(`demande/${id}`);
    return enfantRef.set(newEnfant, {
      merge: true
    });
  }
}
