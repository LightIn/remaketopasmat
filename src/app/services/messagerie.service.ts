import {Injectable} from '@angular/core';
import {AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument} from '@angular/fire/firestore';
import {Conversation, Message, User} from '../models/models';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class MessagerieService {

  constructor(
    public firestore: AngularFirestore,
  ) {

  }


  getConversation(id: string) {

    let Ref: AngularFirestoreCollection<Conversation>;
    let Obs: Observable<Conversation[]>;
    Ref = this.firestore.collection<Conversation>('convs', ref => ref.where('participant', 'array-contains', id));
    Obs = Ref.valueChanges();
    return Obs;
  }


  getLastMessage() {

  }


  getMessages(id: string) {
    let Ref: AngularFirestoreCollection<Message>;
    let Obs: Observable<Message[]>;
    Ref = this.firestore.collection<Message>('messages', ref => ref.where('conv', '==', id).orderBy('date', "desc"));
    Obs = Ref.valueChanges();
    return Obs;
  }


  SendMessage(message: Message) {
    const id = this.firestore.createId();
    this.firestore.doc('messages/' + id).set(message);
    this.UpdateLastMessage(message);
  }

  SendImage() {


  }


  getProfileImage(id: string) {

    let Ref: AngularFirestoreDocument<User>;
    let Obs: Observable<User>;
    Ref = this.firestore.doc<User>('users/' + id);
    Obs = Ref.valueChanges();
    return Obs.pipe(map((value, index) => {

        console.log('serv << url : ' + value.photoURL);
        return value.photoURL;
      })
    );
  }


  getConversationOtherProfile(id: string) {
    const myid = JSON.parse(localStorage.getItem('User')).uid;
    let Ref: AngularFirestoreDocument<Conversation>;
    let Obs: Observable<Conversation>;
    Ref = this.firestore.doc<Conversation>('convs/' + id);
    Obs = Ref.valueChanges();
    return Obs.pipe(map((value, index) => {
      let obs: string;
      value.participant.forEach(participan => {
        if (participan !== myid) {
          obs = participan;
        }
      });
      console.log('serv << profile uid  : ' + obs);
      return obs;
    }));
  }


  UpdateLastMessage(message: Message) {


    const content: string = message.content.slice(0, 34) + '...';
    const dateActuel: Date = new Date();
    this.firestore.doc<Conversation>('convs/' + message.conv).update({
      lastMessage: content,
      lastMessageDate: dateActuel
    });
  }


}
