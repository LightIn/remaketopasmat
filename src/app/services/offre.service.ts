import { Injectable, NgZone } from '@angular/core';
import { AngularFirestore, AngularFirestoreDocument, AngularFirestoreCollection } from '@angular/fire/firestore';

import { Demande } from '../models/models';

import { Router } from '@angular/router';
import { Observable } from 'rxjs';



@Injectable({
  providedIn: 'root'
})
export class OffreService {


  offreData: Demande;


  constructor(
    public firestore: AngularFirestore,
    private router: Router,
    private ngZone: NgZone,

  ) {
    console.log(this.items);
    console.log(this.itemsCollection)
  }

  private itemsCollection: AngularFirestoreCollection<Demande>;
  items: Observable<Demande[]>;

  SetOffre(offre: Demande) {
    const id = this.firestore.createId();
    const offreRef: AngularFirestoreDocument<any> = this.firestore.doc(`demande/${id}`);
    return offreRef.set(offre, {
      merge: true
    });


  }
  GetOffre() {

    let itemsCollection: AngularFirestoreCollection<Demande>;
    let item: Observable<Demande[]>;

    itemsCollection = this.firestore.collection<Demande>('/demande');
    item = itemsCollection.valueChanges();
    item.subscribe(next => console.log('error offre'));
    return item;
  }
}
