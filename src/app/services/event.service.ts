import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreDocument, AngularFirestoreCollection } from '@angular/fire/firestore';

import { Event } from '../models/models';
import {Observable} from 'rxjs';
import { Router } from '@angular/router';


@Injectable({
    providedIn: 'root'
  })

export class EventService{

    
    constructor(public firestore: AngularFirestore){ }

    private itemsCollection: AngularFirestoreCollection<Event>;
    events: Observable<Event[]>;

    GetEvents() {
        this.itemsCollection= this.firestore.collection<Event>('events');
        this.events= this.itemsCollection.valueChanges();
        return this.events;
      }
    
      SetEvent(event: Event){
        const id = this.firestore.createId();
        const eventRef: AngularFirestoreDocument<any> = this.firestore.doc(`events/${id}`);
        return eventRef.set(event, {
          merge: true
        });
    
    
      }

}