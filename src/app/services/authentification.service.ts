import {Injectable} from '@angular/core';
import {AngularFirestore, AngularFirestoreDocument} from '@angular/fire/firestore';
import {AngularFireAuth} from '@angular/fire/auth';
import {User} from '../models/models';
import {Router} from '@angular/router';


@Injectable({
  providedIn: 'root'
})


export class AuthentificationService {


  userData: User;


  constructor(
    public firestore: AngularFirestore,
    public auth: AngularFireAuth,
    private router: Router,
  ) {
    this.auth.onAuthStateChanged(async user => {
      if (user) {
        this.userData = await this.GetUserData(user);
        this.SetUserData();
      } else {
        localStorage.setItem('User', null);
        localStorage.setItem('Auth', null);
      }
    }).catch();
  }


  GetUserData(fireuser): Promise<User> {


    const userRef: AngularFirestoreDocument<User> = this.firestore.collection('users').doc(fireuser.uid);
    let newUser: User;
    return userRef.ref.get().then(snapshot => {
      newUser = {
        uid: fireuser.uid,
        email: snapshot.get('email'),
        emailVerified: snapshot.get('emailVerified'),
        displayName: snapshot.get('displayName'),
        photoURL: snapshot.get('photoURL'),
        age: snapshot.get('age'),
        AssMat: snapshot.get('AssMat'),
        tel: snapshot.get('tel'),
        description: snapshot.get('description'),
        address: snapshot.get('address'),
        sex: snapshot.get('sex'),
        friends: snapshot.get('friends'),
      };
      return newUser;
    });
  }


  SetUserData() {
    localStorage.setItem('User', JSON.stringify(this.userData));
    localStorage.setItem('Auth', JSON.stringify(true));
  }


  SignIn(email, password) {
    return this.auth.signInWithEmailAndPassword(email, password)
      .then(async (result) => {
        this.userData = await this.GetUserData(result.user);
        console.log('UserData =');
        console.log(this.userData);
        this.SetUserData();
        await this.router.navigate(['dashboard']);
      }).catch((error) => {
        window.alert(error.message);
      });
  }


  SignUp(user, password) {
    return this.auth.createUserWithEmailAndPassword(user.email, password)
      .then((result) => {
        this.userData = user;
        this.SetUserData();
        this.firestore.doc(`users/${result.user.uid}`).set(this.userData, {
          merge: true
        });
        this.SendVerificationMail();
      }).catch((error) => {
        window.alert(error.message);
      });
  }


  SignOut() {
    return this.auth.signOut().then(() => {
      localStorage.removeItem('User');
      localStorage.removeItem('Auth');
      this.router.navigate(['']);
    });
  }


  SendVerificationMail() {
    return this.auth.currentUser.then(u => u.sendEmailVerification()).then(() => {
      this.router.navigate(['confirm']);
    });
  }


  SetEmailVerified() {
    this.userData = JSON.parse(localStorage.getItem('User'));
    const uid = this.userData.uid;
    const userRef: AngularFirestoreDocument<any> = this.firestore.doc(`users/${uid}`);
    this.userData.emailVerified = true;
    localStorage.setItem('User', JSON.stringify(this.userData));
    localStorage.setItem('Auth', JSON.stringify(true));
    userRef.set(this.userData, {
      merge: true
    });
  }


  isLoggedIn() {
    this.auth.onAuthStateChanged(async user => {
      if (user) {
        console.log(user.uid);
        this.userData = await this.GetUserData(user.uid);
        this.SetUserData();
      } else {
        localStorage.setItem('User', null);
      }
    }).then(() => {
        return JSON.parse(localStorage.getItem('User')) != null;
      }
    );
  }


  ForgotPassword(passwordResetEmail) {
    return this.auth.sendPasswordResetEmail(passwordResetEmail)
      .then(() => {
        window.alert('Password reset email sent, check your inbox.');
      }).catch((error) => {
        window.alert(error);
      });
  }


}


// this.userData = {
//   uid: a.payload.doc.uid,
//   email: a.payload.doc.email,
//   emailVerified: a.payload.doc.emailVerified,
//   displayName: a.payload.doc.displayName,
//   photoURL: a.payload.doc.photoURL,
//   age: a.payload.doc.age,
//   AssMat: a.payload.doc.AssMat,
//   tel: a.payload.doc.tel,
//   description: a.payload.doc.description,
//   address: a.payload.doc.address,
//   sex: a.payload.doc.sex,
// };
