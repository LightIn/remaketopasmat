export interface User {
  displayName: string;
  email: string;
  emailVerified: boolean;
  photoURL: string;
  uid: string;
  age: number;
  AssMat: boolean;
  tel: string;
  description: string;
  address: string;
  sex: string;
  friends: string[];
}

export interface Demande {
  owner: string;
  nbEnfant: number;
  adresse1: string;
  adresse2: string;
  adresse3: string;
  informations: string;
  date: string;
  horaire: string;
}

export interface Enfant {
  nom: string;
  age: number;
  photoUrl: string;
}


export interface Match {

  owner: string;
  requests: string;

}

//event du planning
export interface Event{
  titre: string;
  description: string;
  debut:string;
  fin:string;
  id: string;
}


export interface Message {
  owner: string;
  content: string;
  date: Date;
  image: string;
  conv: string;
  read: boolean;
}


export interface Conversation {
  participant: string[];
  lastMessage: string;
  lastMessageDate: Date;
  id: string;
}

