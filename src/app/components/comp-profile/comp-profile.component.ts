import {Component, OnInit, Input} from '@angular/core';
import {User} from '../../models/models';

@Component({
  selector: 'app-comp-profile',
  templateUrl: './comp-profile.component.html',
  styleUrls: ['./comp-profile.component.scss']
})
export class CompProfileComponent implements OnInit {

  @Input() profile: User;


  constructor() {
  }

  ngOnInit(): void {
  }

}
