import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-graphique',
  templateUrl: './graphique.component.html',
  styleUrls: ['./graphique.component.scss']
})
export class GraphiqueComponent implements OnInit {

  public barChartOptions = {
    scaleShowVerticalLines: false,
    responsive: true
  };

  public barChartLabels = ['Janvier', 'Fevrier', 'Decembre', 'Mai', '30 fevrier', '21 decembbere 2012', '2020#laPandemie'];

  public barChartType = 'bar';

  public barChartLegend = true;

  public barChartData = [
    {data: [65, 59, 80, 81, 56, 55, 40], label: 'Salaire'},
  ];






  constructor() {
  }




  ngOnInit(): void {
  }

}
