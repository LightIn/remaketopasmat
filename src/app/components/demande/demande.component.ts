import {Component, OnInit} from '@angular/core';
import {FormGroup, FormControl, Validators, FormBuilder} from '@angular/forms';
import {Demande} from '../../models/models';
import {OffreService} from '../../services/offre.service';

@Component({
  selector: 'app-demande',
  templateUrl: './demande.component.html',
  styleUrls: ['./demande.component.scss']
})
export class DemandeComponent implements OnInit {
  form: FormGroup;
  nbEnfant = new FormControl('');
  adresse1 = new FormControl('');
  adresse2 = new FormControl('');
  adresse3 = new FormControl('');
  informations = new FormControl('');
  date = new FormControl('');
  horaire = new FormControl('');

  constructor(private offre: OffreService) {
  }

  Valider() {
    const id = JSON.parse(localStorage.getItem("User")).uid;
    
    const newOffre: Demande = {
      owner: id,
      nbEnfant: this.nbEnfant.value,
      adresse1: this.adresse1.value,
      adresse2: this.adresse2.value,
      adresse3: this.adresse3.value,
      informations: this.informations.value,
      date: this.date.value,
      horaire: this.horaire.value,
    };
    this.offre.SetOffre(newOffre);
  }


  ngOnInit(): void {
  }

}
