import {Component, OnInit, ViewChild, Input} from '@angular/core';
import {MatCalendar} from '@angular/material/datepicker'

@Component({
  selector: 'app-calendrier',
  templateUrl: './calendrier.component.html',
  styleUrls: ['./calendrier.component.scss'],

})
export class CalendrierComponent {
  @ViewChild(MatCalendar) _datePicker: MatCalendar<Date>
  selectedDate: any;
 
  constructor() { }
}
