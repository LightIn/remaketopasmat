import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-grid',
  templateUrl: './grid.component.html',
  styleUrls: ['./grid.component.scss']
})
export class GridComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }
  columnDefs = [
    {headerName: 'Nom', field: 'nom', sortable: true, filter: true, checkboxSelection: true},
    {headerName: 'Enfant', field:'enfant', sortable: true, filter: true},
    {headerName: 'Date', field:'date', sortable: true, filter: true},
    
  ];

  // data qui doit être stockées côté serveur 
  rowData = [
    { nom: 'Jack', enfant: 'Emma', date:'20/05/2020'},
    { nom: 'Chloé', enfant: 'Google', date:'20/05/2020'},
    { nom: 'Claude', enfant: 'Mark', date:'20/05/2020'},
    { nom: 'Jack', enfant: 'Emma', date:'20/05/2020'},
    { nom: 'Chloé', enfant: 'Google', date:'20/05/2020'},
    { nom: 'Claude', enfant: 'Mark', date:'20/05/2020'},
    { nom: 'Jack', enfant: 'Emma', date:'20/05/2020'},
    { nom: 'Chloé', enfant: 'Google', date:'20/05/2020'},
    { nom: 'Claude', enfant: 'Mark', date:'20/05/2020'},
  ]
getSelectedRows(){
    //action
}

}
