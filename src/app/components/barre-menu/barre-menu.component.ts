import {Component, OnInit, ViewChild} from '@angular/core';
import {SideMenuComponent} from '../side-menu/side-menu.component';

@Component({
  selector: 'app-barre-menu',
  templateUrl: 'barre-menu.component.html',
  styleUrls: ['barre-menu.component.scss']
})
export class BarreMenuComponent implements OnInit {


  @ViewChild(SideMenuComponent) child: SideMenuComponent;


  constructor() {
  }


  ngOnInit(): void {

  }


  openNavButton() {
    this.child.openNav();
  }


}
