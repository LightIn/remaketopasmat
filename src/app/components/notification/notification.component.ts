import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-notification',
  templateUrl: 'notification.component.html',
  styleUrls: ['notification.component.scss']
})
export class NotificationComponent implements OnInit {

  @Input() titre: string;
  @Input() icon: number; // 1: demande de garde | 2 : anniveraire | 3 : message
  iconStr: string;
  desc: string;

  constructor() {
    }

  ngOnInit(): void {
    if (this.icon == 1) {
      this.iconStr = `fas fa-exchange-alt fa-2x`;
      this.desc = `Demande de garde de la famille ` + this.titre;
    }
    if (this.icon == 2) {
      this.iconStr = `fas fa-envelope fa-2x`;
      this.desc = `Nouveau message de ` + this.titre;

    }
    if (this.icon == 3) {
      this.iconStr = `fas fa-birthday-cake fa-2x`;
      this.desc = `C'est l'anniversaire de ` + this.titre + ` !`;

    }
  }


}
