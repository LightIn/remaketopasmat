import {Component, Inject, AfterViewInit} from '@angular/core';
import {DOCUMENT} from '@angular/common';
import {AuthentificationService} from '../../services/authentification.service';

@Component({
  selector: 'app-side-menu',
  templateUrl: './side-menu.component.html',
  styleUrls: ['./side-menu.component.scss']
})
export class SideMenuComponent implements AfterViewInit {


  nav: any;
  url: string = JSON.parse(localStorage.getItem('User')).photoURL;


  constructor(
    @Inject(DOCUMENT) private document: Document,
    private auth: AuthentificationService) {

  }

  ngAfterViewInit() {
    this.nav = document.getElementById('mySidenav');
  }


  openNav() {
    console.log('Finalement je louvre');
    this.nav.style.width = '250px';
  }


  closeNav() {
    this.nav.style.width = '0';
  }

  disconnect() {
    this.auth.SignOut();
  }


}
