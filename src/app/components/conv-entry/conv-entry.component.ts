import {Component, Input, OnInit} from '@angular/core';
import {Conversation, User} from "../../models/models";
import {map} from "rxjs/operators";
import {MessagerieService} from "../../services/messagerie.service";
import {MatchingService} from "../../services/matching.service";
import {Observable} from "rxjs";

@Component({
  selector: 'app-conv-entry',
  templateUrl: './conv-entry.component.html',
  styleUrls: ['./conv-entry.component.scss']
})
export class ConvEntryComponent implements OnInit {

  @Input() conv: Conversation;
  userAssoc: User = {
    displayName: null,
    email: null,
    emailVerified: null,
    photoURL: null,
    uid: null,
    age: null,
    AssMat: null,
    tel: null,
    description: null,
    address: null,
    sex: null,
    friends: null,
  };


  constructor(
    private messagerie: MessagerieService,
    private match: MatchingService,
  ) {
  }

  ngOnInit(): void {
    this.getProfile(this.conv.id);
  }


  getProfile(id: string) {
    console.log('conv id : ' + id);
    return this.messagerie.getConversationOtherProfile(id).subscribe(value => {
      console.log('La personne a afficher  id : ' + value);
      this.match.getProfile(value).subscribe(value1 => {
        this.userAssoc = value1;
        console.log(value1);
      });
    });
  }

}
