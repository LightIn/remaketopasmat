import {Component, AfterViewInit} from '@angular/core';
import * as L from 'leaflet';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss']
})
export class MapComponent implements AfterViewInit {

  map;


  constructor() {
  }

  ngAfterViewInit(): void {
    this.createMap();
  }

  createMap() {


    const defaultMap = {
      lat: 46.97014,
      lng: 4.22875,
    };

    const zoomLevel = 5;

    this.map = L.map('map', {
      center: [defaultMap.lat, defaultMap.lng
      ],
      zoom: zoomLevel
    })
    ;

    const mainLayer = L.tileLayer('https://{s}.tile.openstreetmap.fr/osmfr/{z}/{x}/{y}.png', {
      minZoom: 1,
      maxZoom: 20,
      attribution: '&copy; Openstreetmap France | &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
      zIndex: 1
    });


    mainLayer.addTo(this.map);


  }


}
