import { Component, OnInit } from '@angular/core';
import {FormGroup, FormControl, Validators, FormBuilder} from '@angular/forms';
import {EventService} from '../../services/event.service';
import { Event } from '../../models/models';


@Component({
  selector: 'app-form-event',
  templateUrl: './form-event.component.html',
  styleUrls: ['./form-event.component.scss']
})
export class FormEventComponent implements OnInit {
  form: FormGroup;
  titre = new FormControl('');
  description = new FormControl('');
  debut = new FormControl('');
  fin = new FormControl('');

  constructor(private event: EventService) { }

  Ajouter() {
    const newEvent: Event = {
      titre: this.titre.value,
      description: this.description.value,
      debut: this.debut.value,
      fin: this.fin.value,
      id: null,
    }
    this.event.SetEvent(newEvent);
  }

  ngOnInit(): void {
  }

}
