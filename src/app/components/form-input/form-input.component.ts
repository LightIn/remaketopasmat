import {Component, OnInit, Input} from '@angular/core';

@Component({
  selector: 'app-form-input',
  templateUrl: './form-input.component.html',
  styleUrls: ['./form-input.component.scss']
})
export class FormInputComponent implements OnInit {

  @Input() nom: string;
  @Input() value: string;
  @Input() place: string;
  @Input() type: string;
  @Input() icon: string;


  constructor() {
  }

  ngOnInit(): void {
  }

}
