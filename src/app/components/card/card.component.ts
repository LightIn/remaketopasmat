import {Component, OnInit, Input} from '@angular/core';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit {

  @Input() Title: string;
  @Input() Subtitle: string;
  @Input() AvatarLink: string;
  @Input() Content: string;
  @Input() ImageLink: string;
  @Input() AlternativeImage: string;
  @Input() AlternativeAvatar: string;
  @Input() Buttons: any[];


  constructor() {
  }

  ngOnInit(): void {
  }

}
