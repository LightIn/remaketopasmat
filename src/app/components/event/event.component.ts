import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-event',
  templateUrl: './event.component.html',
  styleUrls: ['./event.component.scss']
})
export class EventComponent implements OnInit {

  @Input() titre: string;
  @Input() description: string;
  @Input() debut: string;
  @Input() fin: string;



  constructor() { }

  ngOnInit(): void {
  }

}
