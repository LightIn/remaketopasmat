import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-enfant',
  templateUrl: './enfant.component.html',
  styleUrls: ['./enfant.component.scss']
})
export class EnfantComponent implements OnInit {

  @Input() nom: string;
  @Input() imgUrl: string;
  @Input() age: number;
  constructor() { }

  ngOnInit(): void {
  }

}
