import {Component, Input, OnInit} from '@angular/core';
import {User} from '../../models/models';
import {MessagerieService} from '../../services/messagerie.service';

@Component({
  selector: 'app-message-bulle',
  templateUrl: './message-bulle.component.html',
  styleUrls: ['./message-bulle.component.scss']
})
export class MessageBulleComponent implements OnInit {

  @Input() from: string;
  @Input() message: string;
  @Input() SendAt: string;

  @Input() avatar: string;
  MyId: string = JSON.parse(localStorage.getItem('User')).uid;


  constructor(
    private messagerie: MessagerieService,
  ) {

  }


  ngOnInit(): void {
  }




}
