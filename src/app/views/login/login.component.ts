import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {AuthentificationService} from '../../services/authentification.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {


  email = new FormControl('', [
    Validators.required]);
  password = new FormControl('', [
    Validators.required]);


  constructor(private  route: Router, private auth: AuthentificationService) {
  }


  ngOnInit(): void {

    if (this.auth.isLoggedIn) {
      this.route.navigate(['dashboard']);
    }
    if (!JSON.parse(localStorage.getItem('User')).emailVerified && JSON.parse(localStorage.getItem('Auth'))) {
      this.route.navigate(['confirm']);
    }
  }

  Login() {
    console.log(this.email.value);
    this.auth.SignIn(this.email.value, this.password.value);
  }
}
