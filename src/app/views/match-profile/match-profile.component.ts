import {Component, OnInit} from '@angular/core';
import {Router, ActivatedRoute, ParamMap} from '@angular/router';
import {MatchingService} from '../../services/matching.service';
import {User} from '../../models/models';
import {Observable} from 'rxjs';


@Component({
  selector: 'app-match-profile',
  templateUrl: './match-profile.component.html',
  styleUrls: ['./match-profile.component.scss']
})
export class MatchProfileComponent implements OnInit {


  AlreadyMatch = false;


  profile: User = {
    uid: null,
    email: null,
    emailVerified: null,
    displayName: null,
    photoURL: null,
    age: null,
    AssMat: null,
    tel: null,
    description: null,
    address: null,
    sex: null,
    friends: null
  };


  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private service: MatchingService
  ) {

    let Oprofile: Observable<User>;
    const id = this.route.snapshot.paramMap.get('id');
    console.log(id);
    Oprofile = this.service.getProfile(id);
    Oprofile.subscribe(async m => {
      this.profile = await m;
      console.log(await m);

    });


    this.service.checkIfMachAsk(id).subscribe(next => {
      this.AlreadyMatch = next.length > 0;
    });


  }


  ngOnInit() {
  }


  ask(id: string) {
    console.log('?');
    this.service.askFriend(id, JSON.parse(localStorage.getItem('User')).uid);
  }


  cancel(id: string) {
    this.service.cancelFriend(id, JSON.parse(localStorage.getItem('User')).uid);
  }

}
