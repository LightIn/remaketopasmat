import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { Enfant } from '../../models/models';
import { EnfantsService } from '../../services/enfants.service';

@Component({
  selector: 'app-ajouter-enfant',
  templateUrl: './ajouter-enfant.component.html',
  styleUrls: ['./ajouter-enfant.component.scss']
})
export class AjouterEnfantComponent implements OnInit {

  constructor(private enfant: EnfantsService) { }

  form: FormGroup;
  nom = new FormControl('');
  age = new FormControl('');
  photoUrl = new FormControl('');

  ngOnInit(): void {
  }

  AddEnfant() {
    const newEnfant: Enfant = {
      nom: this.nom.value,
      age: this.age.value,
      photoUrl: this.photoUrl.value,
    };
    this.enfant.AddEnfant(newEnfant);
  }
}
