import {Component, OnInit} from '@angular/core';
import {MatchingService} from '../../services/matching.service';
import {Observable} from 'rxjs';
import {User} from '../../models/models';

@Component({
  selector: 'app-matching',
  templateUrl: './matching.component.html',
  styleUrls: ['./matching.component.scss']
})
export class MatchingComponent implements OnInit {


  poten: Observable<User[]>;
  AssValue: boolean;


  constructor(private match: MatchingService) {
  }


  ngOnInit(): void {
    this.AssValue = JSON.parse(localStorage.getItem('User')).AssMat;
    this.poten = this.match.GetMatchingUser(this.AssValue);
  }


}
