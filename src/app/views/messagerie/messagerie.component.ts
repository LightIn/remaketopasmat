import {Component, OnInit} from '@angular/core';
import {combineLatest, Observable} from 'rxjs';
import {Conversation, User} from '../../models/models';
import {MessagerieService} from '../../services/messagerie.service';
import {MatchingService} from '../../services/matching.service';
import {map, switchMap} from 'rxjs/operators';


interface ConvModule {

  participant: string[];
  id: string;
  lastMessage: string;
  user: User;
}


@Component({
  selector: 'app-messagerie',
  templateUrl: './messagerie.component.html',
  styleUrls: ['./messagerie.component.scss']
})
export class MessagerieComponent implements OnInit {

  constructor(
    private messagerie: MessagerieService,
    private match: MatchingService,
  ) {
  }

  convs: Observable<Conversation[]>;


  ngOnInit(): void {
    const id = JSON.parse(localStorage.getItem('User')).uid;
    this.convs = this.messagerie.getConversation(id);
  }





}
























