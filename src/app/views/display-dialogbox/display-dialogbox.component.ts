import { Component, OnInit, Input } from '@angular/core';
import { MatDialog } from '@angular/material/dialog'
import { DialogBoxComponent } from 'src/app/components/dialog-box/dialog-box.component';

@Component({
  selector: 'app-display-dialogbox',
  templateUrl: './display-dialogbox.component.html',
  styleUrls: ['./display-dialogbox.component.scss']
})
export class DisplayDialogboxComponent implements OnInit {

@Input() buttonName: string;

  constructor(public dialog: MatDialog) { }

    openDialog(){
        const dialogRef = this.dialog.open(DialogBoxComponent);
    }

    ngOnInit(): void {
    }
}



