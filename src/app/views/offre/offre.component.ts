import { Component, OnInit, HostListener } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { OffreService } from '../../services/offre.service';
import { Demande } from '../../models/models';

@Component({
  selector: 'app-offre',
  templateUrl: './offre.component.html',
  styleUrls: ['./offre.component.scss']
})

  
export class OffreComponent implements OnInit {

  listOffre: Observable<Demande[]>;

  constructor(
    public offre: OffreService,
  ) {
    
   
    
  }

  ngOnInit() {
    setTimeout(() => {
      let grids = document.querySelectorAll(".grid > *");
      this.resizeAllGridItems(grids);
      (document.querySelector(".grid") as HTMLElement).style.visibility = 'visible'
    }, 500);

    this.listOffre = this.offre.GetOffre();

  }


  @HostListener('window:resize', ['$event'])
  onResize(event) {
    console.log('resized');
    let grids = document.querySelectorAll(".grid > *");
    this.resizeAllGridItems(grids);
  }


  resizeGridItem(item) {
    let grid = document.getElementsByClassName("grid")[0];
    let rowHeight = parseInt(window.getComputedStyle(grid).getPropertyValue('grid-auto-rows'));
    let rowGap = parseInt(window.getComputedStyle(grid).getPropertyValue('grid-row-gap'));
    let rowSpan = Math.ceil((item.querySelector('.grid > * > *').getBoundingClientRect().height + rowGap) / (rowHeight +
      rowGap));
    item.style.gridRowEnd = "span " + rowSpan;
  }

  resizeAllGridItems(grids: any) {
    grids = document.querySelectorAll(".grid > *");
    let x: number;
    for (x = 0; x < grids.length; x++) {
      this.resizeGridItem(grids[x]);
    }
  }

  resizeInstance(instance) {
    let item = instance.elements[0];
    this.resizeGridItem(item);
  }

}
  


