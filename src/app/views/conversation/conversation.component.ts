import {Component, OnInit, ViewChild} from '@angular/core';
import {AuthentificationService} from '../../services/authentification.service';
import {Location} from '@angular/common';
import {ActivatedRoute, Router} from '@angular/router';
import {MatchingService} from '../../services/matching.service';
import {MessagerieService} from '../../services/messagerie.service';
import {Observable} from 'rxjs';
import {Message, User} from '../../models/models';
import {Form, FormControl, Validators} from '@angular/forms';
import {ElementRef, Renderer2} from '@angular/core';


@Component({
  selector: 'app-conversation',
  templateUrl: './conversation.component.html',
  styleUrls: ['./conversation.component.scss']
})
export class ConversationComponent implements OnInit {

  @ViewChild('input', {read: ElementRef}) el: ElementRef;
  message: FormControl = new FormControl('');
  messages: Observable<Message[]>;
  ConvId: string;
  avatarUs: string = JSON.parse(localStorage.getItem('User')).photoURL;
  avatarThey: string;
  myid = JSON.parse(localStorage.getItem('User')).uid;
  Participant: Observable<User>;


  constructor(private auth: AuthentificationService,
              private location: Location,
              private route: ActivatedRoute,
              private router: Router,
              private service: MatchingService,
              private messagerie: MessagerieService,
              private renderer: Renderer2,
              private match: MatchingService,
  ) {
  }


  ngOnInit(): void {
    this.ConvId = this.route.snapshot.paramMap.get('id');
    this.messages = this.messagerie.getMessages(this.ConvId);
    this.messagerie.getConversationOtherProfile(this.ConvId).subscribe(value => {
      this.Participant = this.match.getProfile(value);
      this.messagerie.getProfileImage(value).subscribe(value1 => {
        console.log('leur avatar : ' + value1);
        this.avatarThey = value1;
      });
    });

  }


  sendMessage() {

    if (this.message.value === '') {
      return null;
    }

    const newMessage: Message = {

      owner: JSON.parse(localStorage.getItem('User')).uid,
      content: this.message.value,
      date: new Date(),
      image: null,
      conv: this.ConvId,
      read: false,
    };
    console.log(this.message.value);
    this.messagerie.SendMessage(newMessage);
    this.message.setValue('');
    console.log(this.message.value);
    this.el.nativeElement.querySelector('input').value = '';
  }


  backClicked() {
    this.location.back();
  }


  OnEnter(event) {

    if (event.keyCode === 13) {
      this.sendMessage();
    }

  }
}
