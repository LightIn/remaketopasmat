
import { Component, OnInit, HostListener } from '@angular/core';

import {Router} from '@angular/router'  ;
import { MatDialog } from '@angular/material/dialog';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {


  ngOnInit() {
    setTimeout( () => {
      const grids = document.querySelectorAll('.grid > *');
      this.resizeAllGridItems(grids);
      (document.querySelector('.grid') as HTMLElement).style.visibility = 'visible';
     }, 500 );

  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    console.log('resized');
    const grids = document.querySelectorAll('.grid > *');
    this.resizeAllGridItems(grids);
}


  resizeGridItem(item) {
    const grid = document.getElementsByClassName('grid')[0];
    const rowHeight = parseInt(window.getComputedStyle(grid).getPropertyValue('grid-auto-rows'));
    const rowGap = parseInt(window.getComputedStyle(grid).getPropertyValue('grid-row-gap'));
    const rowSpan = Math.ceil((item.querySelector('.grid > * > *').getBoundingClientRect().height + rowGap) / (rowHeight +
      rowGap));
    item.style.gridRowEnd = 'span ' + rowSpan;
  }

    resizeAllGridItems(grids: any) {
      grids = document.querySelectorAll('.grid > *');
      let x: number;
      for (x = 0; x < grids.length; x++) {
      this.resizeGridItem(grids[x]);
    }
  }

  resizeInstance(instance) {
    const item = instance.elements[0];
    this.resizeGridItem(item);
  }

}


