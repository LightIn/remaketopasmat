import {Component, OnInit} from '@angular/core';
import {Location} from '@angular/common';
import {User} from '../../models/models';
import {ActivatedRoute, Router} from '@angular/router';
import {MatchingService} from '../../services/matching.service';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-other-profile',
  templateUrl: './other-profile.component.html',
  styleUrls: ['./other-profile.component.scss']
})
export class OtherProfileComponent implements OnInit {


  profile: User = {
    uid: null,
    email: null,
    emailVerified: null,
    displayName: null,
    photoURL: null,
    age: null,
    AssMat: null,
    tel: null,
    description: null,
    address: null,
    sex: null,
    friends: null
  };


  constructor(
    private location: Location,
    private route: ActivatedRoute,
    private router: Router,
    private service: MatchingService
  ) {

    let Oprofile: Observable<User>;
    const id = this.route.snapshot.paramMap.get('id');
    console.log(id);
    Oprofile = this.service.getProfile(id);
    Oprofile.subscribe(async m => {
      this.profile = await m;
      console.log(await m);
    });
  }

  ngOnInit(): void {
  }


  backClicked() {
    this.location.back();
  }
}
