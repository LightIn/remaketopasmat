import {AfterViewInit, Component} from '@angular/core';
import {AuthentificationService} from '../../services/authentification.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-mail-confirm',
  templateUrl: './mail-confirm.component.html',
  styleUrls: ['./mail-confirm.component.scss']
})
export class MailConfirmComponent implements AfterViewInit {

  constructor(private auth: AuthentificationService, private router: Router) {
  }

  ngAfterViewInit(): void {
    this.auth.auth.onAuthStateChanged(user => {
      if (user) {
        const currentUser = this.auth.auth.currentUser;
        if (currentUser != null) {
          currentUser.then(u => {
            if (u.emailVerified) {
              console.log('Verifier');
              this.auth.SetEmailVerified();
              this.router.navigate(['dashboard']);
            }
          });
        }
      }
    });
  }


}
