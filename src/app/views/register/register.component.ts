import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {AuthentificationService} from '../../services/authentification.service';
import {User} from '../../models/models';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {


  username = new FormControl('', Validators.required);
  email = new FormControl('', Validators.required);
  password = new FormControl('', Validators.required);
  repeatpass = new FormControl('', Validators.required);
  Assmat = new FormControl('', Validators.required);
  sex = new FormControl('', Validators.required);
  age = new FormControl('', Validators.required);

  constructor(private auth: AuthentificationService) {
  }

  ngOnInit(): void {


  }

  Register() {
    let assmat: boolean;
    let Ssex: string;
    let Aage: number;

    if (this.Assmat.value) {
      if (this.Assmat.value === '1') {
        assmat = true;
      } else {
        assmat = false;
      }

    } else {
      return alert('Vous devez remplir tout les champs')
    }
    if (this.age.value) {
      console.log(this.age.value);
      if (this.age.value >= 18) {
        Aage = this.age.value;
      } else {
        return alert('vous etes trop jeunne');
      }
    } else {
      return alert('vous devez enter un age');
    }
    if (this.sex.value) {
      if (this.sex.value === '1') {
        Ssex = 'Homme';
      } else {
        Ssex = 'Femme';
      }
    } else {
      return alert('Vous devez remplir tout les champs');
    }

    if (this.password.value === this.repeatpass.value) {

      const user: User = {
        displayName: this.username.value,
        email: this.email.value,
        emailVerified: false,
        photoURL: null,
        uid: null,
        age: Aage,
        AssMat: assmat,
        tel: null,
        description: null,
        address: null,
        sex: Ssex,
        friends: [],
      };
      this.auth.SignUp(user, this.password.value);
    } else {
      alert('le mot de passe doit etre identique les deux fois');
    }
  }


}
