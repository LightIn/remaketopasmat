import {Component, OnInit} from '@angular/core';
import {MatchingService} from '../../services/matching.service';
import {Match, User} from '../../models/models';
import {switchMap} from 'rxjs/operators';
import {combineLatest, Observable} from 'rxjs';
import {Router} from '@angular/router';

@Component({
  selector: 'app-match-accept',
  templateUrl: './match-accept.component.html',
  styleUrls: ['./match-accept.component.scss']
})
export class MatchAcceptComponent implements OnInit {


  match: Observable<Match[]>;
  profiles: User[] = [];


  constructor(private matchingService: MatchingService,
              private router: Router) {
  }

  ngOnInit(): void {
    const uid = JSON.parse(localStorage.getItem('User')).uid;

    this.matchingService.getFriendRequests(uid).pipe(
      switchMap(friendRequests => {
          const obs = friendRequests.map(
            req => {
              console.log('requet : ' + req.requests);
              return this.matchingService.getProfile(req.requests);
            }
          );
          obs.forEach(o => o.subscribe(next => console.log('dans liste obstreval :' + next.displayName)));
          return combineLatest(obs);
        }
      )
    ).subscribe(async next => {
      next.forEach(us => {
        console.log('apref le zip :' + us.displayName);
      });
      this.profiles = next;
    });
  }


  showProfile(id: string) {
    this.router.navigate(['/profile/' + id]);
  }





  addFriend(id: string) {
    console.log('friend add : ' + id);
    this.matchingService.addFriend(id);
  }


}
