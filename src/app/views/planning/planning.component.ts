import { Component, OnInit, Input, ViewChild, AfterViewInit, HostListener } from '@angular/core';
import {EventService} from '../../services/event.service';
import { Observable } from 'rxjs';
import { Event } from '../../models/models';
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';

@Component({
  selector: 'app-planning',
  templateUrl: './planning.component.html',
  styleUrls: ['./planning.component.scss']
})
export class PlanningComponent implements OnInit {
@Input() Mois : string;
@Input() Annee : string;

Jour = ['Lundi' , 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi']
Heure = ['8h', '9h', '10h', '11h', '12h', '13h', '14h', '15h', '16h', '17h']


    events: Observable<Event[]>;

    constructor(private event: EventService, public firestore: AngularFirestore){
        
    }

ngOnInit(): void {
    this.events = this.event.GetEvents();
     }

  

clicButton(){
    console.log('test');
}

delEvent(id: string) {
    if(confirm("Etes vous sûr ?")){
    let event: AngularFirestoreDocument<Event>;
    event = this.firestore.doc<Event>('event/' + id);
    event.delete();
    }
  }

}





