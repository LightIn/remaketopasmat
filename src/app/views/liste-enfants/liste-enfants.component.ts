import { Component, OnInit } from '@angular/core';
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { Enfant } from '../../models/models';
import { EnfantsService } from '../../services/enfants.service';

export interface Item { name: string; }

@Component({
  selector: 'app-liste-enfants',
  templateUrl: './liste-enfants.component.html',
  styleUrls: ['./liste-enfants.component.scss']
})

export class ListeEnfantsComponent implements OnInit {

  enfants: Observable<Enfant[]>;

  constructor(private enfServ: EnfantsService, public firestore: AngularFirestore) {
  };

  ngOnInit(): void {
    this.enfants = this.enfServ.GetEnfants();

  }

  delEnfant(id: string) {
    let enfant: AngularFirestoreDocument<Enfant>;
    enfant = this.firestore.doc<Enfant>('enfants/' + id);
    enfant.delete();
  }

}
