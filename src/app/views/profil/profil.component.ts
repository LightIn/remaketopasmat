import {Component, OnInit} from '@angular/core';
import {User} from '../../models/models';
import {AuthentificationService} from '../../services/authentification.service';
import {AngularFireStorage, AngularFireUploadTask} from '@angular/fire/storage';
import {Observable} from 'rxjs';
import {finalize} from 'rxjs/operators';
import {Router} from '@angular/router';
import {FormControl} from '@angular/forms';
import {AngularFirestoreDocument} from '@angular/fire/firestore';


@Component({
  selector: 'app-profil',
  templateUrl: './profil.component.html',
  styleUrls: ['./profil.component.scss']
})


export class ProfileViewComponent implements OnInit {


  Profil: User = JSON.parse(localStorage.getItem('User'));
  task: AngularFireUploadTask;
  uploadPourcent: Observable<any>;
  file: File = null;
  downloadURL: Observable<string>;


  displayName = new FormControl(this.Profil.displayName);
  email = new FormControl(this.Profil.email);
  age = new FormControl(this.Profil.age);
  sex = new FormControl(this.Profil.sex);
  description = new FormControl(this.Profil.description);
  address = new FormControl(this.Profil.address);
  tel = new FormControl(this.Profil.tel);


  constructor(
    private  storage: AngularFireStorage,
    private authService: AuthentificationService,
    private router: Router
  ) {
  }


  ngOnInit(): void {


  }


  update() {

    console.log('Update');

    const updateUser: User =
      {
        displayName: this.displayName.value,
        email: this.email.value,
        emailVerified: true,
        photoURL: this.Profil.photoURL,
        uid: this.Profil.uid,
        age: this.age.value,
        AssMat: this.Profil.AssMat,
        tel: this.tel.value,
        description: this.description.value,
        address: this.address.value,
        sex: this.sex.value,
        friends: this.Profil.friends,
      };
    localStorage.setItem('User', JSON.stringify(updateUser));
    const userRef: AngularFirestoreDocument<any> = this.authService.firestore.doc(`users/${this.Profil.uid}`);
    return userRef.set(updateUser, {
      merge: true
    });


  }


  startUpload(files: FileList) {
    let url: string;
    this.file = files.item(0);
    if (this.file.type.split('/')[0] !== 'image') {
      console.error('Movais Tipe');
    }
    const path = `picture/${this.authService.userData.uid}`;
    this.task = this.storage.upload(path, this.file);
    const ref = this.storage.ref(path);
    this.uploadPourcent = this.task.percentageChanges();
    this.task.snapshotChanges().pipe(
      finalize(() => {
        this.downloadURL = ref.getDownloadURL();
        this.downloadURL.subscribe(value => {
          url = value;
          console.log('Url vaux : ' + url);
          this.authService.firestore.collection('users').doc(this.authService.userData.uid).set({
            photoURL: url
          }, {merge: true});
          const user = this.authService.userData;
          user.photoURL = url;
          localStorage.setItem('User', JSON.stringify(user));
          this.router.navigate(['profil']);
          alert('Image uploader, reactualiser la page pour l\'afficher');
        });
      })
    ).subscribe();
  }

}



















